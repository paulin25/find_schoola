import { createRouter, createWebHashHistory } from "vue-router";
import Accueil from "../components/Accueil.vue";
import Contact from "../components/Contact.vue";
import Etablissement from "../components/Etablissement.vue";
import Information from "../components/Information.vue";
import Connexion from "../components/Connexion.vue";
import Inscription from "../components/Inscription.vue";
import Jardin from "../components/Jardin.vue"
import Primaire from "../components/Primaire.vue"
import College from "../components/College.vue"
import Lycee from "../components/Lycee.vue"
import Universite from "../components/Universite.vue"
import Commentcamarche from "../components/Commentcamarche.vue"
import FAQ from "../components/FAQ.vue"
import Temoignage from "../components/Temoignage.vue"
import Soring from "../components/Soring.vue"
import HomeView from "../views/HomeView.vue";

const routes = [
  {
    path: "/",
    name: "Accueil",
    component: Accueil,
  },
  {
    path: "/etablissements",
    name: "Etablissement",
    component: Etablissement,
  },
  
  {
    path: "/information",
    name: "Information",
    component: Information,
  },
  {
    path: "/contact",
    name: "Contact",
    component: Contact,
  },
  {
    path: "/connexion",
    name: "Connexion",
    component: Connexion,
  },
  {
    path: "/inscription",
    name: "Inscription",
    component: Inscription,
  },
  {
    path: "/jardin",
    name: "Jardin",
    component: Jardin,
  },
  {
    path: "/primaire",
    name: "Primaire",
    component: Primaire,
  },
  {
    path: "/college",
    name: "College",
    component: College,
  },
  {
    path: "/lycee",
    name: "Lycee",
    component: Lycee,
  },
  {
    path: "/universite",
    name: "Universite",
    component: Universite,
  },
  {
    path: "/commentcamarche",
    name: "Commentcamarche",
    component: Commentcamarche,
  },
  {
    path: "/faq",
    name: "FAQ",
    component: FAQ,
  },
  {
    path: "/temoignage",
    name: "Temoignage",
    component: Temoignage,
  },
  {
    path: "/soring",
    name: "Soring",
    component: Soring,
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
